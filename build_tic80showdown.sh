git clone -b lovebyte --depth 1 https://github.com/nesbox/TIC-80.git
cd lovebyte
git submodule update --depth 1 --init --recursive
cd build
cmake ..
make -j4
strip --strip-debug bin/tic80
cp -a bin/tic80 ../tic80showdown
cd ..
