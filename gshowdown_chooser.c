#include <gtk/gtk.h>

static uint W=240;
static uint H=136;

uint size_limit=256;
uint counter=25;

static void
set_size (GtkWidget *widget,
          gpointer   data)
{
  size_limit=atoi(gtk_combo_box_get_active_id(GTK_COMBO_BOX(widget)));
  g_print ("code limit set to: %d\n",size_limit);
}

static void
set_counter (GtkWidget *widget,
             gpointer   data)
{
  counter=atoi(gtk_combo_box_get_active_id(GTK_COMBO_BOX(widget)));
  g_print ("Counter limit set to: %d\n",counter);
}

static void
launch (GtkWidget *widget,
        gpointer   data)
{
  char tic80_call[512];
  // the parameter --codeexport=showdown1.dat isn't used for load script but need to be passed to have counters working
  snprintf(tic80_call,512,"tic80showdown --skip --codeexport=showdown1.dat --lowerlimit=%u --upperlimit=%u --battletime=%u\n",
		   size_limit, size_limit*2, counter);
  printf(tic80_call);
  system(tic80_call);
}

static void
activate (GtkApplication* app,
          gpointer        user_data)
{
  GtkWidget *window;
  GtkWidget *grid;
  GtkWidget *button,*button1,*button2;

  window = gtk_application_window_new (app);
  gtk_window_set_title (GTK_WINDOW (window), "LoveByte TIC-80 Launcher");
  //gtk_window_set_default_size (GTK_WINDOW (window), W, H);

  grid = gtk_grid_new ();
  gtk_window_set_child (GTK_WINDOW (window), grid);

  button1 = gtk_combo_box_text_new ();
  gtk_combo_box_text_append (GTK_COMBO_BOX_TEXT (button1), "256", "256 bytes");
  gtk_combo_box_text_append (GTK_COMBO_BOX_TEXT (button1), "128", "128 bytes");
  gtk_combo_box_set_active(GTK_COMBO_BOX (button1), 0);
  g_signal_connect (button1, "changed", G_CALLBACK (set_size), NULL);
  gtk_grid_attach (GTK_GRID (grid), button1, 0, 0, 1, 1);

  button2 = gtk_combo_box_text_new ();
  gtk_combo_box_text_append (GTK_COMBO_BOX_TEXT (button2), "25", "25 minutes");
  gtk_combo_box_text_append (GTK_COMBO_BOX_TEXT (button2), "10", "10 minutes");
  gtk_combo_box_set_active(GTK_COMBO_BOX (button2), 0);
  g_signal_connect (button2, "changed", G_CALLBACK (set_counter), NULL);
  gtk_grid_attach (GTK_GRID (grid), button2, 1, 0, 1, 1);

  button = gtk_button_new_with_label ("Launch TIC80");
  g_signal_connect (button, "clicked", G_CALLBACK (launch), NULL);
  gtk_grid_attach (GTK_GRID (grid), button, 0, 1, 2, 1);

  button = gtk_button_new_with_label ("Quit");
  g_signal_connect_swapped (button, "clicked", G_CALLBACK (gtk_window_destroy), window);
  gtk_grid_attach (GTK_GRID (grid), button, 0, 2, 2, 1);

  gtk_widget_show (window);
}

int
main (int    argc,
      char **argv)
{
  GtkApplication *app;
  int status;

  app = gtk_application_new ("org.tic80.lovebytelauncher", G_APPLICATION_FLAGS_NONE);
  g_signal_connect (app, "activate", G_CALLBACK (activate), NULL);
  status = g_application_run (G_APPLICATION (app), argc, argv);
  g_object_unref (app);

  return status;
}
