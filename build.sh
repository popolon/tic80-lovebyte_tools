#!/usr/bin/bash
pkg-config --exists gtk4
rc=$?

if [ ${rc} -eq 0 ]
then
  gcc $( pkg-config --cflags gtk4 ) -o gshowdown_chooser gshowdown_chooser.c $( pkg-config --libs gtk4 )
else
  gcc $( pkg-config --cflags gtk+-3.0 ) -o gshowdown_chooser gshowdown_chooser3.c $( pkg-config --libs gtk+-3.0 )
fi
