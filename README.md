# TIC80-LoveByte_tools

Tools for helping to participate to LoveByte TIC80 256/128bytes coding battles.

# Table of Content

* [gShowdown chooser](#gshowdown-chooser)
   * [On OS that support GTK4](#on-os-that-support-gtk4)
   * [Installing GTK4 on OS that do not support it](#installing-gtk4-on-os-that-do-not-support-it)
   * [GTK3 anyway](#gtk3-anyway)
* [Building Lovebyte version of TIC80 on non-Arch linux distros](#building-lovebyte-version-of-tic80-on-non-arch-linux-distros)

## gShowdown chooser

* gshowdown_chooser: A simple GTK4 (there is a fallback GTK3 version) interface for choosing the kind of battle, only 2 options:
* 128 or 256 bytes
* 10 minutes or 25 minutes

for builing it on non-Arch Linux based distos. Install dependencies:

Prefer GTK4 version if possible, it will be far more efficient.

### On OS that support GTK4

Debian based (need a system from 2021 to have GTK4 on non Arch Linux):
<pre>sudo apt install build-essential libgtk-4-dev</pre>

For all systems but MS-Windows:
<pre>./build.sh</pre>

If you use Microsoft system you need to use special script (untested)
<pre>build4.bat</pre>

I suppose that after installing GTK4 on Windows it should work if this renamed build.bat

### Installing GTK4 on OS that do not support it

You can [install GTK4-4.2+ followinng Beyond LinuxFromScratch book receipe](https://www.linuxfromscratch.org/blfs/view/svn/x/gtk4.html)

Here are some instructions to build GTK4 on Windows: https://www.collabora.com/news-and-blog/blog/2021/03/18/build-and-run-gtk-4-applications-with-visual-studio/

Official documentation about:
* [installing GTK4 on Linux](https://www.gtk.org/docs/installations/linux/)
* [installing GTK4 on MacOS](https://www.gtk.org/docs/installations/macos/)
* [installing GTK4 on Windows](https://www.gtk.org/docs/installations/windows/)

There are [GTK4 bindings for C++, D, JavaScript, Perl, Python, Rust, Scheme and Vala](https://www.gtk.org/docs/language-bindings/) (See left side bare for Scheme).

Documentation about the popular [GTK Python binding](https://pygobject.readthedocs.io/en/latest/).

### GTK3 anyway

But if you still want to use preinstalled GTK3 it's still possible, I backported it to GTK3 (so learned some aspect to port from GTK3 to GTK4 this way):

Debian based:
<pre>sudo apt install build-essential libgtk-3-dev</pre>

For all systems but MS-Windows:
<pre>./build.sh</pre>

If you use Microsoft system you need to use special script (untested)
<pre>build3.bat</pre>

This need the lovebyte battle version of TIC80, I made an AUR package for it's derivated from the PKGBUILD from tic80-git from MaryJaneInChain, you can look at the (PKGBUILD)[https://aur.archlinux.org/cgit/aur.git/tree/PKGBUILD?h=tic-80-lovebyte-git] to follow the quite simple recipe for other Linux distribution. Should work on any architecture (including ARM, and RISC-V)

https://aur.archlinux.org/packages/tic-80-lovebyte-git/

The chooser and the tic-80 LoveByte edition can be used standalone for training. To participate to battle, you need the client.py script, TIC-80 Lovebyte and client.py are working together to stream the democoder actions to the server.

## Building Lovebyte version of TIC80 on non-Arch linux distros

Debian based:

I tested this method on RISC-V 64 version of Debian 11 Bulleye (dev, compiled 2021-05-20), so it should work on most architectures, and looking at the packages, should work on Debian 10 (Buster) too.

<pre>
sudo apt install build-essential git cmake freeglut3-dev libglvnd-dev lua luarocks \
     libsdl2-dev libsdl2-image-dev libsdl2-mixer-dev libsdl2-net-dev libsdl2-ttf-dev
</pre>

You will need [lua-sdl2](https://github.com/Tangent128/luasdl2):
<pre>
luraocks install lua-sdl2
</pre>

The script build_tic80showdown.sh will do it for you and place it in the current directory, you should copy it in you binary path (generally /usr/bin/ or /usr/local/bin/) to be accessible by gshouwdown_chooser

Warning:
* This is not a complet git repository clone, this is only the last HEAD for a quick cloning, and not spent lot of network bandwidth and disk pasce for just one compilatuion. If you want a complete clone remove the "-depth 1" options. It's already about 481MB of code, instead of 1.1GB
* make -j4 use 4 cores, if you have low memory use make or make -j2 instead.
 
<pre>
git clone -b lovebyte --depth 1 https://github.com/nesbox/TIC-80.git
cd lovebyte
git submodule update --depth 1 --init --recursive
cd build
cmake ..
make -j4
</pre>

The binary is now in bin/tic80 you can copy it in /usr/bin/tic80showndown:
<pre>
sudo cp -a bin/tic80 /usr/bin/tic80showndown
</pre>

The executable is around 13MB, if you want a smaller one without debug information it will take only 5MB
<pre>
sudo strip --strip-debug /usr/bin/tic80showdown
</pre>
